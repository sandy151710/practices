import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-hero-child',
  templateUrl: './hero-child.component.html',
  styleUrls: ['./hero-child.component.css']
})
export class HeroChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() hero: String;
  @Input('master') masterName: string;

}
