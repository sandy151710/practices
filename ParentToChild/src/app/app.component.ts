import { Component } from '@angular/core';
import { HEROES } from './hero.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  heroes: HEROES[] = [
    new HEROES("Sandeep", "Ravish"),
    new HEROES("Sandeep", "Sanjiv"),
    new HEROES("Sandeep", "Shubham"),
  ];
}
